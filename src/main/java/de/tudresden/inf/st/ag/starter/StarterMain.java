package de.tudresden.inf.st.ag.starter;
import org.jastadd.ag.ast.*;
import java.util.Scanner;
import java.io.File;
import java.io.IOException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.nio.charset.StandardCharsets;

public class StarterMain {
  public static void main(String[] args){
    System.out.println("Hello, input amount of disks:");
    Hanoi hanoi;
    hanoi = new Hanoi();
    int amountOfDisk = 0;
    while(amountOfDisk <= 0){//input #disks
      Scanner sc=new Scanner(System.in);
      String s=sc.next();
      amountOfDisk = checkInt(s);
    }
    Root root = new Root();
    hanoi.Initialisation(amountOfDisk);
    hanoi.play();
    System.out.println("Game done.");
    hanoi.present();
    }

  public static int checkInt(String str){
    int a =0;
    try {
      a = Integer.parseInt(str);
      } catch (NumberFormatException e) {
        return 0;
      }
    return a;
  }  
}