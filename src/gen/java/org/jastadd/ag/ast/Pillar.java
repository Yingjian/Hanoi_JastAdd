/* This file was generated with JastAdd2 (http://jastadd.org) version 2.3.5 */
package org.jastadd.ag.ast;
import java.util.*;
/**
 * @ast node
 * @declaredat E:\\project\\20211201\\src\\gen\\jastadd\\ag.ast:17
 * @astdecl Pillar : ASTNode ::= Disk*;
 * @production Pillar : {@link ASTNode} ::= <span class="component">{@link Disk}*</span>;

 */
public class Pillar extends ASTNode<ASTNode> implements Cloneable {
  /**
   * @aspect MoveTo
   * @declaredat E:\\project\\20211201\\src\\main\\jastadd\\hanoi\\MoveTo.jadd:63
   */
  public boolean moveTo(Pillar P){
      int i = this.getNumDisk();
      P.addDisk(this.getDisk(i-1));
      this.getDisks().removeChild(i-1);
      return true;
  }
  /**
   * @aspect ReferenceCreation
   * @declaredat E:\\project\\20211201\\src\\gen\\jastadd\\agRefResolver.jadd:195
   */
  public static Pillar createRef(String ref) {
    Unresolved$Pillar unresolvedNode = new Unresolved$Pillar();
    unresolvedNode.setUnresolved$Token(ref);
    unresolvedNode.setUnresolved$ResolveOpposite(true);
    return unresolvedNode;
  }
  /**
   * @aspect ReferenceCreation
   * @declaredat E:\\project\\20211201\\src\\gen\\jastadd\\agRefResolver.jadd:201
   */
  public static Pillar createRefDirection(String ref) {
    Unresolved$Pillar unresolvedNode = new Unresolved$Pillar();
    unresolvedNode.setUnresolved$Token(ref);
    unresolvedNode.setUnresolved$ResolveOpposite(false);
    return unresolvedNode;
  }
  /**
   * @aspect ResolverTrigger
   * @declaredat E:\\project\\20211201\\src\\gen\\jastadd\\agRefResolver.jadd:293
   */
  public void resolveAll() {
    super.resolveAll();
  }
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\20211201\\src\\gen\\jastadd\\agRefResolver.jadd:769
   */
  Unresolved$Node$Interface as$Unresolved() {
    return null;
  }
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\20211201\\src\\gen\\jastadd\\agRefResolver.jadd:775
   */
  boolean is$Unresolved() {
    return false;
  }
  /**
   * @declaredat ASTNode:1
   */
  public Pillar() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  public void init$Children() {
    children = new ASTNode[1];  getChild_handler = new ASTNode$DepGraphNode[children.length];
    state().enterConstruction();
    setChild(new JastAddList(), 0);
    state().exitConstruction();
  }
  /**
   * @declaredat ASTNode:16
   */
  @ASTNodeAnnotation.Constructor(
    name = {"Disk"},
    type = {"JastAddList<Disk>"},
    kind = {"List"}
  )
  public Pillar(JastAddList<Disk> p0) {
state().enterConstruction();
    setChild(p0, 0);
state().exitConstruction();
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:27
   */
  protected int numChildren() {
    
    state().addHandlerDepTo(numChildren_handler);
    return 1;
  }
  /**
   * @apilevel internal
   * @declaredat ASTNode:35
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:39
   */
  public void flushAttrCache() {
  }
  /** @apilevel internal 
   * @declaredat ASTNode:42
   */
  public void flushCollectionCache() {
  }
  /** @apilevel internal 
   * @declaredat ASTNode:45
   */
  public Pillar clone() throws CloneNotSupportedException {
    Pillar node = (Pillar) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:50
   */
  public Pillar copy() {
    try {
      Pillar node = (Pillar) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      node.inc_state = inc_CLONED;
      for (int i = 0; node.children != null && i < node.children.length; i++) {
        node.children[i] = null;
      }
      inc_copyHandlers(node);
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:74
   */
  @Deprecated
  public Pillar fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:84
   */
  public Pillar treeCopyNoTransform() {
    Pillar tree = (Pillar) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.children[i] = child;
          child.parent = tree;
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:105
   */
  public Pillar treeCopy() {
    Pillar tree = (Pillar) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) getChild(i);
        if (child != null) {
          child = child.treeCopy();
          tree.children[i] = child;
          child.parent = tree;
        }
      }
    }
    return tree;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:120
   */
  protected boolean childIsNTA(int index) {
    return super.childIsNTA(index);
  }
  /**
   * @declaredat ASTNode:123
   */
  protected ASTNode$DepGraphNode moveSeq_handler;
  /**
   * @declaredat ASTNode:124
   */
  protected ASTNode$DepGraphNode ID_handler;
  /**
   * @declaredat ASTNode:125
   */
  protected void inc_copyHandlers(Pillar copy) {
    super.inc_copyHandlers(copy);

        if (moveSeq_handler != null) {
          copy.moveSeq_handler = ASTNode$DepGraphNode.createAttrHandler(moveSeq_handler, copy);
        }
        if (ID_handler != null) {
          copy.ID_handler = ASTNode$DepGraphNode.createAttrHandler(ID_handler, copy);
        }
  }
  /** @apilevel internal 
   * @declaredat ASTNode:137
   */
  public void reactToDependencyChange(String attrID, Object _parameters) {
    super.reactToDependencyChange(attrID, _parameters);
  }
  /**
   * @declaredat ASTNode:144
   */
  private boolean inc_throwAway_visited = false;
  /** @apilevel internal 
   * @declaredat ASTNode:146
   */
  public void inc_throwAway() {
  if (inc_throwAway_visited) {
    return;
  }
  inc_throwAway_visited = true;
  inc_state = inc_GARBAGE;
  super.inc_throwAway();
  if (moveSeq_handler != null) {
    moveSeq_handler.throwAway();
  }
  if (ID_handler != null) {
    ID_handler.throwAway();
  }
  inc_throwAway_visited = false;
}
  /**
   * @declaredat ASTNode:161
   */
  private boolean inc_cleanupListeners_visited = false;
  /**
   * @declaredat ASTNode:162
   */
  public void cleanupListeners() {
  if (inc_cleanupListeners_visited) {
    return;
  }
  inc_cleanupListeners_visited = true;
  if (moveSeq_handler != null) {
    moveSeq_handler.cleanupListeners();
  }
  
  if (ID_handler != null) {
    ID_handler.cleanupListeners();
  }
  super.cleanupListeners();
  inc_cleanupListeners_visited = false;
}
  /**
   * @declaredat ASTNode:177
   */
  private boolean inc_cleanupListenersInTree_visited = false;
  /**
   * @declaredat ASTNode:178
   */
  public void cleanupListenersInTree() {
  if (inc_cleanupListenersInTree_visited) {
    return;
  }
  inc_cleanupListenersInTree_visited = true;
  cleanupListeners();
  for (int i = 0; children != null && i < children.length; i++) {
    ASTNode child = children[i];
    if (child == null) {
      continue;
    }
    child.cleanupListenersInTree();
  }
  inc_cleanupListenersInTree_visited = false;
}
  /**
   * Replaces the Disk list.
   * @param list The new list node to be used as the Disk list.
   * @apilevel high-level
   */
  public Pillar setDiskList(JastAddList<Disk> list) {
    setChild(list, 0);
    return this;
  }
  /**
   * Retrieves the number of children in the Disk list.
   * @return Number of children in the Disk list.
   * @apilevel high-level
   */
  public int getNumDisk() {
    return getDiskList().getNumChild();
  }
  /**
   * Retrieves the number of children in the Disk list.
   * Calling this method will not trigger rewrites.
   * @return Number of children in the Disk list.
   * @apilevel low-level
   */
  public int getNumDiskNoTransform() {
    return getDiskListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the Disk list.
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the Disk list.
   * @apilevel high-level
   */
  public Disk getDisk(int i) {
    return (Disk) getDiskList().getChild(i);
  }
  /**
   * Check whether the Disk list has any children.
   * @return {@code true} if it has at least one child, {@code false} otherwise.
   * @apilevel high-level
   */
  public boolean hasDisk() {
    return getDiskList().getNumChild() != 0;
  }
  /**
   * Append an element to the Disk list.
   * @param node The element to append to the Disk list.
   * @apilevel high-level
   */
  public Pillar addDisk(Disk node) {
    JastAddList<Disk> list = (parent == null) ? getDiskListNoTransform() : getDiskList();
    list.addChild(node);
    return this;
  }
  /** @apilevel low-level 
   */
  public Pillar addDiskNoTransform(Disk node) {
    JastAddList<Disk> list = getDiskListNoTransform();
    list.addChild(node);
    return this;
  }
  /**
   * Replaces the Disk list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   */
  public Pillar setDisk(Disk node, int i) {
    JastAddList<Disk> list = getDiskList();
    list.setChild(node, i);
    return this;
  }
  /**
   * Retrieves the Disk list.
   * @return The node representing the Disk list.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.ListChild(name="Disk")
  public JastAddList<Disk> getDiskList() {
    JastAddList<Disk> list = (JastAddList<Disk>) getChild(0);
    return list;
  }
  /**
   * Retrieves the Disk list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the Disk list.
   * @apilevel low-level
   */
  public JastAddList<Disk> getDiskListNoTransform() {
    return (JastAddList<Disk>) getChildNoTransform(0);
  }
  /**
   * @return the element at index {@code i} in the Disk list without
   * triggering rewrites.
   */
  public Disk getDiskNoTransform(int i) {
    return (Disk) getDiskListNoTransform().getChildNoTransform(i);
  }
  /**
   * Retrieves the Disk list.
   * @return The node representing the Disk list.
   * @apilevel high-level
   */
  public JastAddList<Disk> getDisks() {
    return getDiskList();
  }
  /**
   * Retrieves the Disk list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the Disk list.
   * @apilevel low-level
   */
  public JastAddList<Disk> getDisksNoTransform() {
    return getDiskListNoTransform();
  }
  /**
   * @attribute inh
   * @aspect CanMove
   * @declaredat E:\\project\\20211201\\src\\main\\jastadd\\hanoi\\CanMove.jrag:12
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="CanMove", declaredAt="E:\\project\\20211201\\src\\main\\jastadd\\hanoi\\CanMove.jrag:12")
  public int moveSeq() {
    ASTState state = state();
    
    if (moveSeq_handler == null) {
      moveSeq_handler = new ASTNode$DepGraphNode(this, "moveSeq", null, ASTNode.inc_EMPTY) {
        @Override public void reactToDependencyChange() {
          {
            moveSeq_computed = null;
            moveSeq_handler.notifyDependencies();
            Pillar.this.state().trace().flushIncAttr(Pillar.this, "moveSeq", "", "");
          }
        }
      };
    }
    state().addHandlerDepTo(moveSeq_handler);
    
    
    
    
    
    if (moveSeq_computed == ASTState.NON_CYCLE || moveSeq_computed == state().cycle()) {
      state().trace().cacheRead(this, "Pillar.moveSeq()", "", moveSeq_value);
      return moveSeq_value;
    }
    
    state().enterAttrStoreEval(moveSeq_handler);
    moveSeq_value = getParent().Define_moveSeq(this, null);
    if (state().inCircle()) {
      moveSeq_computed = state().cycle();
      state().trace().cacheWrite(this, "Pillar.moveSeq()", "", moveSeq_value);
    } else {
      moveSeq_computed = ASTState.NON_CYCLE;
      state().trace().cacheWrite(this, "Pillar.moveSeq()", "", moveSeq_value);
    }
    
    state().exitAttrStoreEval(moveSeq_handler);
    
    
    
    
    
    
    return moveSeq_value;
  }
  /** @apilevel internal */
  private void moveSeq_reset() {
    state().trace().flushAttr(this, "Pillar.moveSeq()", "", moveSeq_value);
    moveSeq_computed = null;
  }
  /** @apilevel internal */
  protected ASTState.Cycle moveSeq_computed = null;

  /** @apilevel internal */
  protected int moveSeq_value;

  /**
   * @attribute inh
   * @aspect CanMove
   * @declaredat E:\\project\\20211201\\src\\main\\jastadd\\hanoi\\CanMove.jrag:20
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="CanMove", declaredAt="E:\\project\\20211201\\src\\main\\jastadd\\hanoi\\CanMove.jrag:20")
  public int ID() {
    ASTState state = state();
    
    if (ID_handler == null) {
      ID_handler = new ASTNode$DepGraphNode(this, "ID", null, ASTNode.inc_EMPTY) {
        @Override public void reactToDependencyChange() {
          {
            ID_computed = null;
            ID_handler.notifyDependencies();
            Pillar.this.state().trace().flushIncAttr(Pillar.this, "ID", "", "");
          }
        }
      };
    }
    state().addHandlerDepTo(ID_handler);
    
    
    
    
    
    if (ID_computed == ASTState.NON_CYCLE || ID_computed == state().cycle()) {
      state().trace().cacheRead(this, "Pillar.ID()", "", ID_value);
      return ID_value;
    }
    
    state().enterAttrStoreEval(ID_handler);
    ID_value = getParent().Define_ID(this, null);
    if (state().inCircle()) {
      ID_computed = state().cycle();
      state().trace().cacheWrite(this, "Pillar.ID()", "", ID_value);
    } else {
      ID_computed = ASTState.NON_CYCLE;
      state().trace().cacheWrite(this, "Pillar.ID()", "", ID_value);
    }
    
    state().exitAttrStoreEval(ID_handler);
    
    
    
    
    
    
    return ID_value;
  }
  /** @apilevel internal */
  private void ID_reset() {
    state().trace().flushAttr(this, "Pillar.ID()", "", ID_value);
    ID_computed = null;
  }
  /** @apilevel internal */
  protected ASTState.Cycle ID_computed = null;

  /** @apilevel internal */
  protected int ID_value;

  /** @apilevel internal */
  public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
  /** @apilevel internal */
  public boolean canRewrite() {
    return false;
  }

}
