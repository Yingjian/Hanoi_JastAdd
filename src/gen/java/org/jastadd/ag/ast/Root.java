/* This file was generated with JastAdd2 (http://jastadd.org) version 2.3.5 */
package org.jastadd.ag.ast;
import java.util.*;
/**
 * @ast node
 * @declaredat E:\\project\\20211201\\src\\gen\\jastadd\\ag.ast:1
 * @astdecl Root : ASTNode ::= BinaryConstraint* UnaryConstraint* Atom*;
 * @production Root : {@link ASTNode} ::= <span class="component">{@link BinaryConstraint}*</span> <span class="component">{@link UnaryConstraint}*</span> <span class="component">{@link Atom}*</span>;

 */
public class Root extends ASTNode<ASTNode> implements Cloneable {
  /**
   * @aspect MoveTo
   * @declaredat E:\\project\\20211201\\src\\main\\jastadd\\hanoi\\MoveTo.jadd:2
   */
  public int check(Pillar P0, Pillar P1, boolean turn){
/*for every odd turn(turn==true), move the smallest disk D0 in sequence.
Therefore, CheckD0 checks for a move from P0 to P1, when D0 is on the top of P0.*/
    if(turn==true){
      CheckD0 C=new CheckD0();
      this.addAtom(C);
      this.getAtom(0).addRel(P0);
      if(this.getAtom(0).eval()){
        return 1;//odd turn and valid move
      }else{
        return 0;
      }
    }
/*for every even turn(turn==false), move the disk D on top of P0 to P1:
D is not D0(the smallest disk).
There is always one D can be moved in the even turn according to the game rules.
Statements:
1.P0 is not empty: not OnoDisk().
2.When condition 1 holds, P1 can be empty: TnoDisk().
3.When 1 holds and 2 doesn\u9225\u6a9b hold, D0 < D1(for the disks on the top of P0 and P1 respectively).
Therefore, we want not OnoDisk() and (TnoDisk() or CheckSize()) to be true.

Conjunction()-------------Negation()--------------OnoDisk()
                      |
                      |--------------------Disjunction()-----------TnoDisk()
                                              |
                                              |------------------CheckSize()*/
    Conjunction C0=new Conjunction();
    Negation C1=new Negation();
    Disjunction C2=new Disjunction();
    OnoDisk C3=new OnoDisk();
    TnoDisk C4=new TnoDisk();
    CheckSize C5=new CheckSize();

    C0.setLeft(C1);
    C0.setRight(C2);
    C1.setConstraint(C3);
    C2.setLeft(C4);
    C2.setRight(C5);

    C3.addRel(P0);
    C4.addRel(P1);
    C5.addRel(P0);
    C5.addRel(P1);
    this.addBinaryConstraint(C0);
    if(this.getBinaryConstraint(0).eval()){
      return 2;//even turn and valid move
    }
    return 0;
  }
  /**
   * @aspect ReferenceCreation
   * @declaredat E:\\project\\20211201\\src\\gen\\jastadd\\agRefResolver.jadd:3
   */
  public static Root createRef(String ref) {
    Unresolved$Root unresolvedNode = new Unresolved$Root();
    unresolvedNode.setUnresolved$Token(ref);
    unresolvedNode.setUnresolved$ResolveOpposite(true);
    return unresolvedNode;
  }
  /**
   * @aspect ReferenceCreation
   * @declaredat E:\\project\\20211201\\src\\gen\\jastadd\\agRefResolver.jadd:9
   */
  public static Root createRefDirection(String ref) {
    Unresolved$Root unresolvedNode = new Unresolved$Root();
    unresolvedNode.setUnresolved$Token(ref);
    unresolvedNode.setUnresolved$ResolveOpposite(false);
    return unresolvedNode;
  }
  /**
   * @aspect ResolverTrigger
   * @declaredat E:\\project\\20211201\\src\\gen\\jastadd\\agRefResolver.jadd:228
   */
  public void resolveAll() {
    super.resolveAll();
  }
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\20211201\\src\\gen\\jastadd\\agRefResolver.jadd:321
   */
  Unresolved$Node$Interface as$Unresolved() {
    return null;
  }
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\20211201\\src\\gen\\jastadd\\agRefResolver.jadd:327
   */
  boolean is$Unresolved() {
    return false;
  }
  /**
   * @declaredat ASTNode:1
   */
  public Root() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  public void init$Children() {
    children = new ASTNode[3];  getChild_handler = new ASTNode$DepGraphNode[children.length];
    state().enterConstruction();
    setChild(new JastAddList(), 0);
    setChild(new JastAddList(), 1);
    setChild(new JastAddList(), 2);
    state().exitConstruction();
  }
  /**
   * @declaredat ASTNode:18
   */
  @ASTNodeAnnotation.Constructor(
    name = {"BinaryConstraint", "UnaryConstraint", "Atom"},
    type = {"JastAddList<BinaryConstraint>", "JastAddList<UnaryConstraint>", "JastAddList<Atom>"},
    kind = {"List", "List", "List"}
  )
  public Root(JastAddList<BinaryConstraint> p0, JastAddList<UnaryConstraint> p1, JastAddList<Atom> p2) {
state().enterConstruction();
    setChild(p0, 0);
    setChild(p1, 1);
    setChild(p2, 2);
state().exitConstruction();
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:31
   */
  protected int numChildren() {
    
    state().addHandlerDepTo(numChildren_handler);
    return 3;
  }
  /**
   * @apilevel internal
   * @declaredat ASTNode:39
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:43
   */
  public void flushAttrCache() {
  }
  /** @apilevel internal 
   * @declaredat ASTNode:46
   */
  public void flushCollectionCache() {
  }
  /** @apilevel internal 
   * @declaredat ASTNode:49
   */
  public Root clone() throws CloneNotSupportedException {
    Root node = (Root) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:54
   */
  public Root copy() {
    try {
      Root node = (Root) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      node.inc_state = inc_CLONED;
      for (int i = 0; node.children != null && i < node.children.length; i++) {
        node.children[i] = null;
      }
      inc_copyHandlers(node);
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:78
   */
  @Deprecated
  public Root fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:88
   */
  public Root treeCopyNoTransform() {
    Root tree = (Root) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.children[i] = child;
          child.parent = tree;
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:109
   */
  public Root treeCopy() {
    Root tree = (Root) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) getChild(i);
        if (child != null) {
          child = child.treeCopy();
          tree.children[i] = child;
          child.parent = tree;
        }
      }
    }
    return tree;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:124
   */
  protected boolean childIsNTA(int index) {
    return super.childIsNTA(index);
  }
  /**
   * @declaredat ASTNode:127
   */
  protected void inc_copyHandlers(Root copy) {
    super.inc_copyHandlers(copy);

  }
  /** @apilevel internal 
   * @declaredat ASTNode:133
   */
  public void reactToDependencyChange(String attrID, Object _parameters) {
    super.reactToDependencyChange(attrID, _parameters);
  }
  /**
   * @declaredat ASTNode:140
   */
  private boolean inc_throwAway_visited = false;
  /** @apilevel internal 
   * @declaredat ASTNode:142
   */
  public void inc_throwAway() {
  if (inc_throwAway_visited) {
    return;
  }
  inc_throwAway_visited = true;
  inc_state = inc_GARBAGE;
  super.inc_throwAway();
  inc_throwAway_visited = false;
}
  /**
   * @declaredat ASTNode:151
   */
  private boolean inc_cleanupListeners_visited = false;
  /**
   * @declaredat ASTNode:152
   */
  public void cleanupListeners() {
  if (inc_cleanupListeners_visited) {
    return;
  }
  inc_cleanupListeners_visited = true;
  super.cleanupListeners();
  inc_cleanupListeners_visited = false;
}
  /**
   * @declaredat ASTNode:160
   */
  private boolean inc_cleanupListenersInTree_visited = false;
  /**
   * @declaredat ASTNode:161
   */
  public void cleanupListenersInTree() {
  if (inc_cleanupListenersInTree_visited) {
    return;
  }
  inc_cleanupListenersInTree_visited = true;
  cleanupListeners();
  for (int i = 0; children != null && i < children.length; i++) {
    ASTNode child = children[i];
    if (child == null) {
      continue;
    }
    child.cleanupListenersInTree();
  }
  inc_cleanupListenersInTree_visited = false;
}
  /**
   * Replaces the BinaryConstraint list.
   * @param list The new list node to be used as the BinaryConstraint list.
   * @apilevel high-level
   */
  public Root setBinaryConstraintList(JastAddList<BinaryConstraint> list) {
    setChild(list, 0);
    return this;
  }
  /**
   * Retrieves the number of children in the BinaryConstraint list.
   * @return Number of children in the BinaryConstraint list.
   * @apilevel high-level
   */
  public int getNumBinaryConstraint() {
    return getBinaryConstraintList().getNumChild();
  }
  /**
   * Retrieves the number of children in the BinaryConstraint list.
   * Calling this method will not trigger rewrites.
   * @return Number of children in the BinaryConstraint list.
   * @apilevel low-level
   */
  public int getNumBinaryConstraintNoTransform() {
    return getBinaryConstraintListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the BinaryConstraint list.
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the BinaryConstraint list.
   * @apilevel high-level
   */
  public BinaryConstraint getBinaryConstraint(int i) {
    return (BinaryConstraint) getBinaryConstraintList().getChild(i);
  }
  /**
   * Check whether the BinaryConstraint list has any children.
   * @return {@code true} if it has at least one child, {@code false} otherwise.
   * @apilevel high-level
   */
  public boolean hasBinaryConstraint() {
    return getBinaryConstraintList().getNumChild() != 0;
  }
  /**
   * Append an element to the BinaryConstraint list.
   * @param node The element to append to the BinaryConstraint list.
   * @apilevel high-level
   */
  public Root addBinaryConstraint(BinaryConstraint node) {
    JastAddList<BinaryConstraint> list = (parent == null) ? getBinaryConstraintListNoTransform() : getBinaryConstraintList();
    list.addChild(node);
    return this;
  }
  /** @apilevel low-level 
   */
  public Root addBinaryConstraintNoTransform(BinaryConstraint node) {
    JastAddList<BinaryConstraint> list = getBinaryConstraintListNoTransform();
    list.addChild(node);
    return this;
  }
  /**
   * Replaces the BinaryConstraint list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   */
  public Root setBinaryConstraint(BinaryConstraint node, int i) {
    JastAddList<BinaryConstraint> list = getBinaryConstraintList();
    list.setChild(node, i);
    return this;
  }
  /**
   * Retrieves the BinaryConstraint list.
   * @return The node representing the BinaryConstraint list.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.ListChild(name="BinaryConstraint")
  public JastAddList<BinaryConstraint> getBinaryConstraintList() {
    JastAddList<BinaryConstraint> list = (JastAddList<BinaryConstraint>) getChild(0);
    return list;
  }
  /**
   * Retrieves the BinaryConstraint list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the BinaryConstraint list.
   * @apilevel low-level
   */
  public JastAddList<BinaryConstraint> getBinaryConstraintListNoTransform() {
    return (JastAddList<BinaryConstraint>) getChildNoTransform(0);
  }
  /**
   * @return the element at index {@code i} in the BinaryConstraint list without
   * triggering rewrites.
   */
  public BinaryConstraint getBinaryConstraintNoTransform(int i) {
    return (BinaryConstraint) getBinaryConstraintListNoTransform().getChildNoTransform(i);
  }
  /**
   * Retrieves the BinaryConstraint list.
   * @return The node representing the BinaryConstraint list.
   * @apilevel high-level
   */
  public JastAddList<BinaryConstraint> getBinaryConstraints() {
    return getBinaryConstraintList();
  }
  /**
   * Retrieves the BinaryConstraint list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the BinaryConstraint list.
   * @apilevel low-level
   */
  public JastAddList<BinaryConstraint> getBinaryConstraintsNoTransform() {
    return getBinaryConstraintListNoTransform();
  }
  /**
   * Replaces the UnaryConstraint list.
   * @param list The new list node to be used as the UnaryConstraint list.
   * @apilevel high-level
   */
  public Root setUnaryConstraintList(JastAddList<UnaryConstraint> list) {
    setChild(list, 1);
    return this;
  }
  /**
   * Retrieves the number of children in the UnaryConstraint list.
   * @return Number of children in the UnaryConstraint list.
   * @apilevel high-level
   */
  public int getNumUnaryConstraint() {
    return getUnaryConstraintList().getNumChild();
  }
  /**
   * Retrieves the number of children in the UnaryConstraint list.
   * Calling this method will not trigger rewrites.
   * @return Number of children in the UnaryConstraint list.
   * @apilevel low-level
   */
  public int getNumUnaryConstraintNoTransform() {
    return getUnaryConstraintListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the UnaryConstraint list.
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the UnaryConstraint list.
   * @apilevel high-level
   */
  public UnaryConstraint getUnaryConstraint(int i) {
    return (UnaryConstraint) getUnaryConstraintList().getChild(i);
  }
  /**
   * Check whether the UnaryConstraint list has any children.
   * @return {@code true} if it has at least one child, {@code false} otherwise.
   * @apilevel high-level
   */
  public boolean hasUnaryConstraint() {
    return getUnaryConstraintList().getNumChild() != 0;
  }
  /**
   * Append an element to the UnaryConstraint list.
   * @param node The element to append to the UnaryConstraint list.
   * @apilevel high-level
   */
  public Root addUnaryConstraint(UnaryConstraint node) {
    JastAddList<UnaryConstraint> list = (parent == null) ? getUnaryConstraintListNoTransform() : getUnaryConstraintList();
    list.addChild(node);
    return this;
  }
  /** @apilevel low-level 
   */
  public Root addUnaryConstraintNoTransform(UnaryConstraint node) {
    JastAddList<UnaryConstraint> list = getUnaryConstraintListNoTransform();
    list.addChild(node);
    return this;
  }
  /**
   * Replaces the UnaryConstraint list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   */
  public Root setUnaryConstraint(UnaryConstraint node, int i) {
    JastAddList<UnaryConstraint> list = getUnaryConstraintList();
    list.setChild(node, i);
    return this;
  }
  /**
   * Retrieves the UnaryConstraint list.
   * @return The node representing the UnaryConstraint list.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.ListChild(name="UnaryConstraint")
  public JastAddList<UnaryConstraint> getUnaryConstraintList() {
    JastAddList<UnaryConstraint> list = (JastAddList<UnaryConstraint>) getChild(1);
    return list;
  }
  /**
   * Retrieves the UnaryConstraint list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the UnaryConstraint list.
   * @apilevel low-level
   */
  public JastAddList<UnaryConstraint> getUnaryConstraintListNoTransform() {
    return (JastAddList<UnaryConstraint>) getChildNoTransform(1);
  }
  /**
   * @return the element at index {@code i} in the UnaryConstraint list without
   * triggering rewrites.
   */
  public UnaryConstraint getUnaryConstraintNoTransform(int i) {
    return (UnaryConstraint) getUnaryConstraintListNoTransform().getChildNoTransform(i);
  }
  /**
   * Retrieves the UnaryConstraint list.
   * @return The node representing the UnaryConstraint list.
   * @apilevel high-level
   */
  public JastAddList<UnaryConstraint> getUnaryConstraints() {
    return getUnaryConstraintList();
  }
  /**
   * Retrieves the UnaryConstraint list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the UnaryConstraint list.
   * @apilevel low-level
   */
  public JastAddList<UnaryConstraint> getUnaryConstraintsNoTransform() {
    return getUnaryConstraintListNoTransform();
  }
  /**
   * Replaces the Atom list.
   * @param list The new list node to be used as the Atom list.
   * @apilevel high-level
   */
  public Root setAtomList(JastAddList<Atom> list) {
    setChild(list, 2);
    return this;
  }
  /**
   * Retrieves the number of children in the Atom list.
   * @return Number of children in the Atom list.
   * @apilevel high-level
   */
  public int getNumAtom() {
    return getAtomList().getNumChild();
  }
  /**
   * Retrieves the number of children in the Atom list.
   * Calling this method will not trigger rewrites.
   * @return Number of children in the Atom list.
   * @apilevel low-level
   */
  public int getNumAtomNoTransform() {
    return getAtomListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the Atom list.
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the Atom list.
   * @apilevel high-level
   */
  public Atom getAtom(int i) {
    return (Atom) getAtomList().getChild(i);
  }
  /**
   * Check whether the Atom list has any children.
   * @return {@code true} if it has at least one child, {@code false} otherwise.
   * @apilevel high-level
   */
  public boolean hasAtom() {
    return getAtomList().getNumChild() != 0;
  }
  /**
   * Append an element to the Atom list.
   * @param node The element to append to the Atom list.
   * @apilevel high-level
   */
  public Root addAtom(Atom node) {
    JastAddList<Atom> list = (parent == null) ? getAtomListNoTransform() : getAtomList();
    list.addChild(node);
    return this;
  }
  /** @apilevel low-level 
   */
  public Root addAtomNoTransform(Atom node) {
    JastAddList<Atom> list = getAtomListNoTransform();
    list.addChild(node);
    return this;
  }
  /**
   * Replaces the Atom list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   */
  public Root setAtom(Atom node, int i) {
    JastAddList<Atom> list = getAtomList();
    list.setChild(node, i);
    return this;
  }
  /**
   * Retrieves the Atom list.
   * @return The node representing the Atom list.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.ListChild(name="Atom")
  public JastAddList<Atom> getAtomList() {
    JastAddList<Atom> list = (JastAddList<Atom>) getChild(2);
    return list;
  }
  /**
   * Retrieves the Atom list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the Atom list.
   * @apilevel low-level
   */
  public JastAddList<Atom> getAtomListNoTransform() {
    return (JastAddList<Atom>) getChildNoTransform(2);
  }
  /**
   * @return the element at index {@code i} in the Atom list without
   * triggering rewrites.
   */
  public Atom getAtomNoTransform(int i) {
    return (Atom) getAtomListNoTransform().getChildNoTransform(i);
  }
  /**
   * Retrieves the Atom list.
   * @return The node representing the Atom list.
   * @apilevel high-level
   */
  public JastAddList<Atom> getAtoms() {
    return getAtomList();
  }
  /**
   * Retrieves the Atom list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the Atom list.
   * @apilevel low-level
   */
  public JastAddList<Atom> getAtomsNoTransform() {
    return getAtomListNoTransform();
  }
  /** @apilevel internal */
  public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
  /** @apilevel internal */
  public boolean canRewrite() {
    return false;
  }

}
