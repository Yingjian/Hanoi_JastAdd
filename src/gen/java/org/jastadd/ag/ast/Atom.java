/* This file was generated with JastAdd2 (http://jastadd.org) version 2.3.5 */
package org.jastadd.ag.ast;
import java.util.*;
/**
 * @ast node
 * @declaredat E:\\project\\20211201\\src\\gen\\jastadd\\ag.ast:3
 * @astdecl Atom : Constraint ::= <_impl_Rel:java.util.List<Pillar>>;
 * @production Atom : {@link Constraint} ::= <span class="component">&lt;_impl_Rel:{@link java.util.List<Pillar>}&gt;</span>;

 */
public abstract class Atom extends Constraint implements Cloneable {
  /**
   * @aspect RelAstAPI
   * @declaredat E:\\project\\20211201\\src\\gen\\jastadd\\ag.jadd:4
   */
  public java.util.List<Pillar> getRels() {
    return getRelList();
  }
  /**
   * @aspect RelAstAPI
   * @declaredat E:\\project\\20211201\\src\\gen\\jastadd\\ag.jadd:7
   */
  public java.util.List<Pillar> getRelList() {
    java.util.List<Pillar> l = get_impl_Rel();
    if (l != null) {
      boolean changed = false;
      for (int i = 0; i < l.size(); i++) {
        Pillar element = l.get(i);
        if (element.is$Unresolved()) {
          changed = true;
          Pillar resolvedElement = resolveRelByToken(element.as$Unresolved().getUnresolved$Token(), i);
          l.set(i, resolvedElement);
        }
      }
      if (changed) {
        set_impl_Rel(l);
      }
    }
    return l != null ? java.util.Collections.unmodifiableList(l) : java.util.Collections.emptyList();
  }
  /**
   * @aspect RelAstAPI
   * @declaredat E:\\project\\20211201\\src\\gen\\jastadd\\ag.jadd:25
   */
  public void addRel(Pillar o) {
    assertNotNull(o);
    java.util.List<Pillar> list = tokenjava_util_List_Pillar___impl_Rel;
    if (list == null) {
      list = new java.util.ArrayList<>();
    }
    list.add(o);
    set_impl_Rel(list);
  }
  /**
   * @aspect RelAstAPI
   * @declaredat E:\\project\\20211201\\src\\gen\\jastadd\\ag.jadd:34
   */
  public void addRel(int index, Pillar o) {
    assertNotNull(o);
    java.util.List<Pillar> list = tokenjava_util_List_Pillar___impl_Rel;
    if (list == null) {
      list = new java.util.ArrayList<>();
    }
    list.add(index, o);
    set_impl_Rel(list);
  }
  /**
   * @aspect RelAstAPI
   * @declaredat E:\\project\\20211201\\src\\gen\\jastadd\\ag.jadd:43
   */
  public void removeRel(Pillar o) {
    assertNotNull(o);
    java.util.List<Pillar> list = tokenjava_util_List_Pillar___impl_Rel;
    if (list != null && list.remove(o)) {
      set_impl_Rel(list);
    }
  }
  /**
   * @aspect ReferenceCreation
   * @declaredat E:\\project\\20211201\\src\\gen\\jastadd\\agRefResolver.jadd:27
   */
  public static Atom createRef(String ref) {
    Unresolved$CheckSize unresolvedNode = new Unresolved$CheckSize();
    unresolvedNode.setUnresolved$Token(ref);
    unresolvedNode.setUnresolved$ResolveOpposite(true);
    return unresolvedNode;
  }
  /**
   * @aspect ReferenceCreation
   * @declaredat E:\\project\\20211201\\src\\gen\\jastadd\\agRefResolver.jadd:33
   */
  public static Atom createRefDirection(String ref) {
    Unresolved$CheckSize unresolvedNode = new Unresolved$CheckSize();
    unresolvedNode.setUnresolved$Token(ref);
    unresolvedNode.setUnresolved$ResolveOpposite(false);
    return unresolvedNode;
  }
  /**
   * @aspect ResolverTrigger
   * @declaredat E:\\project\\20211201\\src\\gen\\jastadd\\agRefResolver.jadd:236
   */
  public void resolveAll() {
    getRelList();
    super.resolveAll();
  }
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\20211201\\src\\gen\\jastadd\\agRefResolver.jadd:377
   */
  Unresolved$Node$Interface as$Unresolved() {
    return null;
  }
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\20211201\\src\\gen\\jastadd\\agRefResolver.jadd:383
   */
  boolean is$Unresolved() {
    return false;
  }
  /**
   * @declaredat ASTNode:1
   */
  public Atom() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  public void init$Children() {
    state().enterConstruction();
    state().exitConstruction();
  }
  /**
   * @declaredat ASTNode:14
   */
  @ASTNodeAnnotation.Constructor(
    name = {"_impl_Rel"},
    type = {"java.util.List<Pillar>"},
    kind = {"Token"}
  )
  public Atom(java.util.List<Pillar> p0) {
state().enterConstruction();
    set_impl_Rel(p0);
state().exitConstruction();
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:25
   */
  protected int numChildren() {
    
    state().addHandlerDepTo(numChildren_handler);
    return 0;
  }
  /**
   * @apilevel internal
   * @declaredat ASTNode:33
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:37
   */
  public void flushAttrCache() {
  }
  /** @apilevel internal 
   * @declaredat ASTNode:40
   */
  public void flushCollectionCache() {
  }
  /** @apilevel internal 
   * @declaredat ASTNode:43
   */
  public Atom clone() throws CloneNotSupportedException {
    Atom node = (Atom) super.clone();
    return node;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:54
   */
  @Deprecated
  public abstract Atom fullCopy();
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:62
   */
  public abstract Atom treeCopyNoTransform();
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:70
   */
  public abstract Atom treeCopy();
  /** @apilevel internal 
   * @declaredat ASTNode:72
   */
  protected boolean childIsNTA(int index) {
    return super.childIsNTA(index);
  }
  /**
   * @declaredat ASTNode:75
   */
  protected void inc_copyHandlers(Atom copy) {
    super.inc_copyHandlers(copy);

        if (get_impl_Rel_handler != null) {
          copy.get_impl_Rel_handler = ASTNode$DepGraphNode.createAstHandler(get_impl_Rel_handler, copy);
        }
  }
  /** @apilevel internal 
   * @declaredat ASTNode:84
   */
  public void reactToDependencyChange(String attrID, Object _parameters) {
    super.reactToDependencyChange(attrID, _parameters);
  }
  /**
   * @declaredat ASTNode:91
   */
  private boolean inc_throwAway_visited = false;
  /** @apilevel internal 
   * @declaredat ASTNode:93
   */
  public void inc_throwAway() {
  if (inc_throwAway_visited) {
    return;
  }
  inc_throwAway_visited = true;
  inc_state = inc_GARBAGE;
  super.inc_throwAway();
  if (get_impl_Rel_handler != null) {
    get_impl_Rel_handler.throwAway();
  }
  inc_throwAway_visited = false;
}
  /**
   * @declaredat ASTNode:105
   */
  private boolean inc_cleanupListeners_visited = false;
  /**
   * @declaredat ASTNode:106
   */
  public void cleanupListeners() {
  if (inc_cleanupListeners_visited) {
    return;
  }
  inc_cleanupListeners_visited = true;
  if (get_impl_Rel_handler != null) {
    get_impl_Rel_handler.cleanupListeners();
  }
  super.cleanupListeners();
  inc_cleanupListeners_visited = false;
}
  /**
   * @declaredat ASTNode:117
   */
  private boolean inc_cleanupListenersInTree_visited = false;
  /**
   * @declaredat ASTNode:118
   */
  public void cleanupListenersInTree() {
  if (inc_cleanupListenersInTree_visited) {
    return;
  }
  inc_cleanupListenersInTree_visited = true;
  cleanupListeners();
  for (int i = 0; children != null && i < children.length; i++) {
    ASTNode child = children[i];
    if (child == null) {
      continue;
    }
    child.cleanupListenersInTree();
  }
  inc_cleanupListenersInTree_visited = false;
}
  /**
   */
  protected ASTNode$DepGraphNode get_impl_Rel_handler = ASTNode$DepGraphNode.createAstHandler(this, "get_impl_Rel", null);
  /**
   * Replaces the lexeme _impl_Rel.
   * @param value The new value for the lexeme _impl_Rel.
   * @apilevel high-level
   */
  public Atom set_impl_Rel(java.util.List<Pillar> value) {
    tokenjava_util_List_Pillar___impl_Rel = value;
    
    if (state().disableDeps == 0 && !state().IN_ATTR_STORE_EVAL) {
      get_impl_Rel_handler.notifyDependencies();
    
    
    
    
    }
    return this;
  }
  /** @apilevel internal 
   */
  protected java.util.List<Pillar> tokenjava_util_List_Pillar___impl_Rel;
  /**
   * Retrieves the value for the lexeme _impl_Rel.
   * @return The value for the lexeme _impl_Rel.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="_impl_Rel")
  public java.util.List<Pillar> get_impl_Rel() {
    
    state().addHandlerDepTo(get_impl_Rel_handler);
    return tokenjava_util_List_Pillar___impl_Rel;
  }
  /**
   * @attribute syn
   * @aspect RefResolverStubs
   * @declaredat E:\\project\\20211201\\src\\gen\\jastadd\\agResolverStubs.jrag:6
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="RefResolverStubs", declaredAt="E:\\project\\20211201\\src\\gen\\jastadd\\agResolverStubs.jrag:6")
  public Pillar resolveRelByToken(String id, int position) {
    {
        // default to context-independent name resolution
        return globallyResolvePillarByToken(id);
      }
  }
  /** @apilevel internal */
  public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
  /** @apilevel internal */
  public boolean canRewrite() {
    return false;
  }

}
