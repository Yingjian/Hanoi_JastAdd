/* This file was generated with JastAdd2 (http://jastadd.org) version 2.3.5 */
package org.jastadd.ag.ast;
import java.util.*;
/**
 * @ast node
 * @declaredat E:\\project\\20211201\\src\\gen\\jastadd\\ag.ast:13
 * @astdecl TnoDisk : Atom ::= <_impl_Rel:java.util.List<Pillar>>;
 * @production TnoDisk : {@link Atom};

 */
public class TnoDisk extends Atom implements Cloneable {
  /**
   * @aspect ReferenceCreation
   * @declaredat E:\\project\\20211201\\src\\gen\\jastadd\\agRefResolver.jadd:147
   */
  public static TnoDisk createRef(String ref) {
    Unresolved$TnoDisk unresolvedNode = new Unresolved$TnoDisk();
    unresolvedNode.setUnresolved$Token(ref);
    unresolvedNode.setUnresolved$ResolveOpposite(true);
    return unresolvedNode;
  }
  /**
   * @aspect ReferenceCreation
   * @declaredat E:\\project\\20211201\\src\\gen\\jastadd\\agRefResolver.jadd:153
   */
  public static TnoDisk createRefDirection(String ref) {
    Unresolved$TnoDisk unresolvedNode = new Unresolved$TnoDisk();
    unresolvedNode.setUnresolved$Token(ref);
    unresolvedNode.setUnresolved$ResolveOpposite(false);
    return unresolvedNode;
  }
  /**
   * @aspect ResolverTrigger
   * @declaredat E:\\project\\20211201\\src\\gen\\jastadd\\agRefResolver.jadd:277
   */
  public void resolveAll() {
    super.resolveAll();
  }
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\20211201\\src\\gen\\jastadd\\agRefResolver.jadd:657
   */
  Unresolved$Node$Interface as$Unresolved() {
    return null;
  }
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\20211201\\src\\gen\\jastadd\\agRefResolver.jadd:663
   */
  boolean is$Unresolved() {
    return false;
  }
  /**
   * @declaredat ASTNode:1
   */
  public TnoDisk() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  public void init$Children() {
    state().enterConstruction();
    state().exitConstruction();
  }
  /**
   * @declaredat ASTNode:14
   */
  @ASTNodeAnnotation.Constructor(
    name = {"_impl_Rel"},
    type = {"java.util.List<Pillar>"},
    kind = {"Token"}
  )
  public TnoDisk(java.util.List<Pillar> p0) {
state().enterConstruction();
    set_impl_Rel(p0);
state().exitConstruction();
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:25
   */
  protected int numChildren() {
    
    state().addHandlerDepTo(numChildren_handler);
    return 0;
  }
  /**
   * @apilevel internal
   * @declaredat ASTNode:33
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:37
   */
  public void flushAttrCache() {
  }
  /** @apilevel internal 
   * @declaredat ASTNode:40
   */
  public void flushCollectionCache() {
  }
  /** @apilevel internal 
   * @declaredat ASTNode:43
   */
  public TnoDisk clone() throws CloneNotSupportedException {
    TnoDisk node = (TnoDisk) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:48
   */
  public TnoDisk copy() {
    try {
      TnoDisk node = (TnoDisk) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      node.inc_state = inc_CLONED;
      for (int i = 0; node.children != null && i < node.children.length; i++) {
        node.children[i] = null;
      }
      inc_copyHandlers(node);
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:72
   */
  @Deprecated
  public TnoDisk fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:82
   */
  public TnoDisk treeCopyNoTransform() {
    TnoDisk tree = (TnoDisk) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.children[i] = child;
          child.parent = tree;
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:103
   */
  public TnoDisk treeCopy() {
    TnoDisk tree = (TnoDisk) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) getChild(i);
        if (child != null) {
          child = child.treeCopy();
          tree.children[i] = child;
          child.parent = tree;
        }
      }
    }
    return tree;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:118
   */
  protected boolean childIsNTA(int index) {
    return super.childIsNTA(index);
  }
  /**
   * @declaredat ASTNode:121
   */
  protected ASTNode$DepGraphNode eval_handler;
  /**
   * @declaredat ASTNode:122
   */
  protected void inc_copyHandlers(TnoDisk copy) {
    super.inc_copyHandlers(copy);

        if (eval_handler != null) {
          copy.eval_handler = ASTNode$DepGraphNode.createAttrHandler(eval_handler, copy);
        }
  }
  /** @apilevel internal 
   * @declaredat ASTNode:131
   */
  public void reactToDependencyChange(String attrID, Object _parameters) {
    super.reactToDependencyChange(attrID, _parameters);
  }
  /**
   * @declaredat ASTNode:138
   */
  private boolean inc_throwAway_visited = false;
  /** @apilevel internal 
   * @declaredat ASTNode:140
   */
  public void inc_throwAway() {
  if (inc_throwAway_visited) {
    return;
  }
  inc_throwAway_visited = true;
  inc_state = inc_GARBAGE;
  super.inc_throwAway();
  if (eval_handler != null) {
    eval_handler.throwAway();
  }
  inc_throwAway_visited = false;
}
  /**
   * @declaredat ASTNode:152
   */
  private boolean inc_cleanupListeners_visited = false;
  /**
   * @declaredat ASTNode:153
   */
  public void cleanupListeners() {
  if (inc_cleanupListeners_visited) {
    return;
  }
  inc_cleanupListeners_visited = true;
  if (get_impl_Rel_handler != null) {
    get_impl_Rel_handler.cleanupListeners();
  }
  if (eval_handler != null) {
    eval_handler.cleanupListeners();
  }
  super.cleanupListeners();
  inc_cleanupListeners_visited = false;
}
  /**
   * @declaredat ASTNode:167
   */
  private boolean inc_cleanupListenersInTree_visited = false;
  /**
   * @declaredat ASTNode:168
   */
  public void cleanupListenersInTree() {
  if (inc_cleanupListenersInTree_visited) {
    return;
  }
  inc_cleanupListenersInTree_visited = true;
  cleanupListeners();
  for (int i = 0; children != null && i < children.length; i++) {
    ASTNode child = children[i];
    if (child == null) {
      continue;
    }
    child.cleanupListenersInTree();
  }
  inc_cleanupListenersInTree_visited = false;
}
  /**
   */
  protected ASTNode$DepGraphNode get_impl_Rel_handler = ASTNode$DepGraphNode.createAstHandler(this, "get_impl_Rel", null);
  /**
   * Replaces the lexeme _impl_Rel.
   * @param value The new value for the lexeme _impl_Rel.
   * @apilevel high-level
   */
  public TnoDisk set_impl_Rel(java.util.List<Pillar> value) {
    tokenjava_util_List_Pillar___impl_Rel = value;
    
    if (state().disableDeps == 0 && !state().IN_ATTR_STORE_EVAL) {
      get_impl_Rel_handler.notifyDependencies();
    
    
    
    
    }
    return this;
  }
  /**
   * Retrieves the value for the lexeme _impl_Rel.
   * @return The value for the lexeme _impl_Rel.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="_impl_Rel")
  public java.util.List<Pillar> get_impl_Rel() {
    
    state().addHandlerDepTo(get_impl_Rel_handler);
    return tokenjava_util_List_Pillar___impl_Rel;
  }
  /** @apilevel internal */
  private void eval_reset() {
    state().trace().flushAttr(this, "Constraint.eval()", "", eval_value);
    eval_computed = null;
  }
  /** @apilevel internal */
  protected ASTState.Cycle eval_computed = null;

  /** @apilevel internal */
  protected boolean eval_value;

  /**
   * @attribute syn
   * @aspect Constraints
   * @declaredat E:\\project\\20211201\\src\\main\\jastadd\\hanoi\\Constraints.jrag:32
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Constraints", declaredAt="E:\\project\\20211201\\src\\main\\jastadd\\hanoi\\Constraints.jrag:3")
  public boolean eval() {
    ASTState state = state();
    
    if (eval_handler == null) {
      eval_handler = new ASTNode$DepGraphNode(this, "eval", null, ASTNode.inc_EMPTY) {
        @Override public void reactToDependencyChange() {
          {
            eval_computed = null;
            eval_handler.notifyDependencies();
            TnoDisk.this.state().trace().flushIncAttr(TnoDisk.this, "eval", "", "");
          }
        }
      };
    }
    state().addHandlerDepTo(eval_handler);
    
    
    
    
    
    if (eval_computed == ASTState.NON_CYCLE || eval_computed == state().cycle()) {
      state().trace().cacheRead(this, "Constraint.eval()", "", eval_value);
      return eval_value;
    }
    
    state().enterAttrStoreEval(eval_handler);
    eval_value = eval_compute();
    if (state().inCircle()) {
      eval_computed = state().cycle();
      state().trace().cacheWrite(this, "Constraint.eval()", "", eval_value);
    } else {
      eval_computed = ASTState.NON_CYCLE;
      state().trace().cacheWrite(this, "Constraint.eval()", "", eval_value);
    }
    
    state().exitAttrStoreEval(eval_handler);
    
    
    
    
    
    
    return eval_value;
  }
  /** @apilevel internal */
  private boolean eval_compute() {
          //the target pillar is empty?
          return this.getRels().get(0).getNumDisk()==0;
      }
  /** @apilevel internal */
  public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
  /** @apilevel internal */
  public boolean canRewrite() {
    return false;
  }

}
