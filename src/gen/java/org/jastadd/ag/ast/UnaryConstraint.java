/* This file was generated with JastAdd2 (http://jastadd.org) version 2.3.5 */
package org.jastadd.ag.ast;
import java.util.*;
/**
 * @ast node
 * @declaredat E:\\project\\20211201\\src\\gen\\jastadd\\ag.ast:4
 * @astdecl UnaryConstraint : Constraint ::= Constraint;
 * @production UnaryConstraint : {@link Constraint} ::= <span class="component">{@link Constraint}</span>;

 */
public abstract class UnaryConstraint extends Constraint implements Cloneable {
  /**
   * @aspect ReferenceCreation
   * @declaredat E:\\project\\20211201\\src\\gen\\jastadd\\agRefResolver.jadd:39
   */
  public static UnaryConstraint createRef(String ref) {
    Unresolved$Negation unresolvedNode = new Unresolved$Negation();
    unresolvedNode.setUnresolved$Token(ref);
    unresolvedNode.setUnresolved$ResolveOpposite(true);
    return unresolvedNode;
  }
  /**
   * @aspect ReferenceCreation
   * @declaredat E:\\project\\20211201\\src\\gen\\jastadd\\agRefResolver.jadd:45
   */
  public static UnaryConstraint createRefDirection(String ref) {
    Unresolved$Negation unresolvedNode = new Unresolved$Negation();
    unresolvedNode.setUnresolved$Token(ref);
    unresolvedNode.setUnresolved$ResolveOpposite(false);
    return unresolvedNode;
  }
  /**
   * @aspect ResolverTrigger
   * @declaredat E:\\project\\20211201\\src\\gen\\jastadd\\agRefResolver.jadd:241
   */
  public void resolveAll() {
    super.resolveAll();
  }
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\20211201\\src\\gen\\jastadd\\agRefResolver.jadd:405
   */
  Unresolved$Node$Interface as$Unresolved() {
    return null;
  }
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\20211201\\src\\gen\\jastadd\\agRefResolver.jadd:411
   */
  boolean is$Unresolved() {
    return false;
  }
  /**
   * @declaredat ASTNode:1
   */
  public UnaryConstraint() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  public void init$Children() {
    children = new ASTNode[1];  getChild_handler = new ASTNode$DepGraphNode[children.length];
    state().enterConstruction();
    state().exitConstruction();
  }
  /**
   * @declaredat ASTNode:15
   */
  @ASTNodeAnnotation.Constructor(
    name = {"Constraint"},
    type = {"Constraint"},
    kind = {"Child"}
  )
  public UnaryConstraint(Constraint p0) {
state().enterConstruction();
    setChild(p0, 0);
state().exitConstruction();
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:26
   */
  protected int numChildren() {
    
    state().addHandlerDepTo(numChildren_handler);
    return 1;
  }
  /**
   * @apilevel internal
   * @declaredat ASTNode:34
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:38
   */
  public void flushAttrCache() {
  }
  /** @apilevel internal 
   * @declaredat ASTNode:41
   */
  public void flushCollectionCache() {
  }
  /** @apilevel internal 
   * @declaredat ASTNode:44
   */
  public UnaryConstraint clone() throws CloneNotSupportedException {
    UnaryConstraint node = (UnaryConstraint) super.clone();
    return node;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:55
   */
  @Deprecated
  public abstract UnaryConstraint fullCopy();
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:63
   */
  public abstract UnaryConstraint treeCopyNoTransform();
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:71
   */
  public abstract UnaryConstraint treeCopy();
  /** @apilevel internal 
   * @declaredat ASTNode:73
   */
  protected boolean childIsNTA(int index) {
    return super.childIsNTA(index);
  }
  /**
   * @declaredat ASTNode:76
   */
  protected void inc_copyHandlers(UnaryConstraint copy) {
    super.inc_copyHandlers(copy);

  }
  /** @apilevel internal 
   * @declaredat ASTNode:82
   */
  public void reactToDependencyChange(String attrID, Object _parameters) {
    super.reactToDependencyChange(attrID, _parameters);
  }
  /**
   * @declaredat ASTNode:89
   */
  private boolean inc_throwAway_visited = false;
  /** @apilevel internal 
   * @declaredat ASTNode:91
   */
  public void inc_throwAway() {
  if (inc_throwAway_visited) {
    return;
  }
  inc_throwAway_visited = true;
  inc_state = inc_GARBAGE;
  super.inc_throwAway();
  inc_throwAway_visited = false;
}
  /**
   * @declaredat ASTNode:100
   */
  private boolean inc_cleanupListeners_visited = false;
  /**
   * @declaredat ASTNode:101
   */
  public void cleanupListeners() {
  if (inc_cleanupListeners_visited) {
    return;
  }
  inc_cleanupListeners_visited = true;
  super.cleanupListeners();
  inc_cleanupListeners_visited = false;
}
  /**
   * @declaredat ASTNode:109
   */
  private boolean inc_cleanupListenersInTree_visited = false;
  /**
   * @declaredat ASTNode:110
   */
  public void cleanupListenersInTree() {
  if (inc_cleanupListenersInTree_visited) {
    return;
  }
  inc_cleanupListenersInTree_visited = true;
  cleanupListeners();
  for (int i = 0; children != null && i < children.length; i++) {
    ASTNode child = children[i];
    if (child == null) {
      continue;
    }
    child.cleanupListenersInTree();
  }
  inc_cleanupListenersInTree_visited = false;
}
  /**
   * Replaces the Constraint child.
   * @param node The new node to replace the Constraint child.
   * @apilevel high-level
   */
  public UnaryConstraint setConstraint(Constraint node) {
    setChild(node, 0);
    return this;
  }
  /**
   * Retrieves the Constraint child.
   * @return The current node used as the Constraint child.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Child(name="Constraint")
  public Constraint getConstraint() {
    return (Constraint) getChild(0);
  }
  /**
   * Retrieves the Constraint child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the Constraint child.
   * @apilevel low-level
   */
  public Constraint getConstraintNoTransform() {
    return (Constraint) getChildNoTransform(0);
  }
  /** @apilevel internal */
  public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
  /** @apilevel internal */
  public boolean canRewrite() {
    return false;
  }

}
